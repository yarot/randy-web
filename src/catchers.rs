use {
    rocket::Request,
    rocket_contrib::templates::Template,
};

#[catch(404)]
pub fn not_found(req: &Request<'_>) -> Template {
    let data = json!({
        "title": "Error: 404",
        "path": req.uri().path(),
        "parent": "shared/layout",
    });

    Template::render("shared/error/404", &data)
}
