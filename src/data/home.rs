/// Data structure for "Home" pages
#[derive(Deserialize, Serialize)]
pub struct HomeData {
    /// the page title
    pub title: String,
    /// the page description/one-liner
    pub description: String,
    /// keywords describing the page
    pub keywords: Vec<String>,
    /// the parent template
    pub parent: String,
}
