#![feature(decl_macro, proc_macro_hygiene)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde;
#[macro_use]
extern crate serde_json;

extern crate rocket_contrib;

pub mod catchers;
pub mod data;
pub mod helpers;

#[doc(hidden)]
pub mod pages;

use {
    self::pages::*,
    rocket_contrib::{serve::StaticFiles, templates::Template},
};

fn main() {
    let rock = rocket::ignite()
        .register(catchers![catchers::not_found])
        .mount("/", routes![home::index, home::about, home::stream])
        .mount("/collections", routes![collections::index])
        .mount("/", StaticFiles::from("wwwroot"))
        .attach(Template::custom(|eng| {
            eng.handlebars
                .register_helper("wow", Box::new(helpers::wow_helper));
        }));

    rock.launch();
}

#[cfg(test)]
mod tests {
    use std::io::Result;

    /// Render test for Handlebars templates
    #[test]
    fn render() -> Result<()> {
        Ok(())
    }
}
