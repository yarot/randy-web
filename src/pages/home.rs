use {
    rocket_contrib::{
        templates::Template
    },
    crate::{
        data::home::HomeData,
    }
};

#[get("/")]
pub fn index() -> Template {
    let data = HomeData {
        title: "Home".to_string(),
        description: "".to_string(),
        keywords: vec![],
        parent: "shared/layout".to_string(),
    };

    Template::render("home/index", &data)
}

#[get("/about")]
pub fn about() -> Template {
    let data = HomeData {
        title: "About".to_string(),
        description: "Learn about me and Blue Nebula Studios".to_string(),
        keywords: vec![
            "informational".to_string(),
            "blue nebula studios".to_string(),
            "randy".to_string()
        ],
        parent: "shared/layout".to_string(),
    };

    Template::render("home/about", &data)
}

#[get("/stream")]
pub fn stream() -> Template {
    let data = HomeData {
        title: "Stream".to_string(),
        description: "".to_string(),
        keywords: vec![],
        parent: "shared/layout".to_string(),
    };

    Template::render("home/stream", &data)
}
