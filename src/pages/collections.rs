use {
    rocket_contrib::{
        templates::Template
    },
    crate::{
        data::home::HomeData,
    }
};

#[get("/")]
pub fn index() -> Template {
    let data = HomeData {
        title: "My Collections".to_string(),
        description: "Find my art, past streams, and more".to_string(),
        keywords: vec![
            "art".to_string(),
            "videos".to_string()
        ],
        parent: "shared/layout".to_string()
    };

    Template::render("collections/index", &data)
}
